import { NextFunction, Request, Response } from 'express';
import UserService from '../services/users.service';
import AuthService from '../services/auth.service';
import { GeneralResponse } from '../utils/response.js';
import bcrypt from 'bcrypt';
import error from '../utils/error';
import { Auth } from '../interface/auth.interface';
import { generateAuthToken } from '../helper/user.helper';
const hashRound = 10;

class AuthController{
    public userService = new UserService();
    public authService = new AuthService();

    public userLogin = async (req: Request, res: Response, next: NextFunction) => {
        try {
            
            const hashPassword = await bcrypt.hash(req.body.password,hashRound);
            await this.userService.isUserExist(req.body.username, async (err: any, response: Auth[]) => {
                const validPassword = bcrypt.compare(req.body.password, response[0].password);
                if(err || response.length === 0 ) {
                    next(new error.UnAuthorized('Invalid userId or Password'));
                }else if(!validPassword) {
                    next(new error.UnAuthorized('Invalid userId or Password'));
                }else{
                    const token = generateAuthToken(response[0]);
                    
                    req.body.password = hashPassword;
                    
                    await this.authService.create(req.body, (errAuth : any, responseAuth: any) => {
                        if(!errAuth){
                            next(new GeneralResponse('Authorized user',token));
                        }else{
                            next(new error.GeneralError('User Login failed'));
                        }
                    })
                    
                }
            });
        } catch (err) {
            next(new error.GeneralError('User Login failed.'));   
        }
    }

}

export default AuthController;