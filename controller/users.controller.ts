import { NextFunction, Request, Response } from 'express';
import UserService from '../services/users.service';
import { GeneralResponse } from '../utils/response.js';
import { User } from '../interface/users.interface';
import config from '../utils/config.js';
import bcrypt from 'bcrypt';
import error from '../utils/error';
import { CreateUserDto } from '../dtos/users.dto';
const hashRound = 10; 
const getError = 'Error while getting user';

class UserController {
    public userService = new UserService();

    public registerUser = async (req: Request, res: Response, next: NextFunction) => {
        try {
            
            req.body.password = await bcrypt.hash(req.body.password,hashRound);
            const userData: CreateUserDto = req.body;
            
            await this.userService.create(userData, (err: any, response: object) => {
                if(err) {
                    next(new error.GeneralError('user registeration failed'));
                }else{
                    next(new GeneralResponse('user successfully registered',undefined,config.HTTP_CREATED));
                }
            });
        } catch (err) {
            next(new error.GeneralError('user registeration failed'));        }
    }
    
    public getUserByEmail = async (req: Request, res: Response, next: NextFunction) => {
        const email : string = req.params.email_id;
        try {
            await this.userService.isUserExist(email,(err : any, response: User[]) => {
                if(typeof response != 'undefined' && response.length === 0){
                    next(new error.NotFound('no users found'));
                } else if(err) {
                    next(new error.GeneralError('Error while getting user'));
                } else {
                    next(new GeneralResponse('User details found',response));
                }
            });
        } catch (err) {
            next(new error.GeneralError(getError));
        }
    }
    
}

export default UserController;
