import { User } from '../interface/users.interface';


export class UserModel implements User {
    public id!: number;
    public name!: string;
    public surname!: string;
    public email_id !: string;
    public password !: string;
    public mobile !: string;
    public gender !: string;
  
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
  }