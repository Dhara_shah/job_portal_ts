export interface User {
    id: number;
    email_id: string;
    password: string;
    name: string;
    surname: string;
    mobile: string;
    gender: string;
}
