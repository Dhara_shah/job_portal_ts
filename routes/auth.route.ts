import { Router } from 'express';
import AuthController from '../controller/auth.controller';
import { Routes } from '../interface/routes.interface';
import  {validate} from '../helper/validator.helper.js';
import { authSchema } from '../validation/auth.validation';

class AuthRoute implements Routes {
    public path = '/login';
    public router = Router();
    public authController = new AuthController();
    constructor() {
      this.initializeCategoryRoutes();
    }
    
    public initializeCategoryRoutes() {
      this.router.post(`${this.path}`, [ validate.body( authSchema.schemaLogin ) ], this.authController.userLogin);
    }
  }

  
export default AuthRoute;