import { Router } from 'express';
import UserController from '../controller/users.controller';
import { Routes } from '../interface/routes.interface';
import  { validate } from '../helper/validator.helper.js';
import {user} from '../validation/user.validation';
import { validateEmail } from '../helper/validemail.helper';


class UserRoute implements Routes {
    public path = '/users';
    public router = Router();
    public userController = new UserController();
    constructor() {
      this.initializeCategoryRoutes();
    }
    
    public initializeCategoryRoutes() {
      this.router.get(`${this.path}/:email`, this.userController.getUserByEmail);
      this.router.post(`${this.path}`, [ validateEmail , validate.body( user.schemaUser ) ], this.userController.registerUser);
    }
  }

  
export default UserRoute;
