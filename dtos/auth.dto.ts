import { IsString ,IsEmail } from 'class-validator';

export class CreateAuthDto {
    @IsEmail()
    public email_id: string | undefined;
    @IsString()
    public password: string | undefined;
}