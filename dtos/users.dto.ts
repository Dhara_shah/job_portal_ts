import { IsString ,IsEmail } from 'class-validator';

export class CreateUserDto {
    @IsEmail()
    public email_id: string | undefined;
    @IsString()
    public name: string | undefined;
    @IsString()
    public surname: string | undefined;
    @IsString()
    public mobile: string | undefined;
    @IsString()
    public password: string | undefined;
    @IsString()
    public gender: string | undefined;
  
}