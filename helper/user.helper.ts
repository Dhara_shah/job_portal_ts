import winston from 'winston';
import jwt from 'jsonwebtoken';
import { Auth } from '../interface/auth.interface';
import config from 'config'

const { jwtPrivateKey , TOKEN_EXPIRY } = config.get('jwt');


export const generateAuthToken = (user: Auth) => {
    return jwt.sign({ id : user.id,email_id: user.username},jwtPrivateKey,{expiresIn: TOKEN_EXPIRY}
    );
};
