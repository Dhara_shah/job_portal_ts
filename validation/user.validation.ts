import Joi from 'joi';

const minEmail = 3;
const maxEmail = 320;
const maxPass = 16;
const minPass = 8;
const minMobile = 10;

export const user = {
    schemaUser: Joi.object({
        name: Joi.string().required().empty().messages({
            'string.base': `name should be a type of 'text'`,
            'string.empty': `name cannot be an empty field`,
            'any.required': `name is a required field`}
        ),
        surname: Joi.string().required().empty().messages({
            'string.base': `surname should be a type of 'text'`,
            'string.empty': `surname cannot be an empty field`,
            'any.required': `surname is a required field`}
        ),
        email_id: Joi.string().min(minEmail).max(maxEmail).required().email().empty().messages({
            'string.base': `email_id should be a type of 'text'`,
            'string.empty': `email_id cannot be an empty field`,
            'string.email': `email_id format not valid`,
            'any.required': `email_id is a required field`}
        ),
        password: Joi.string().min(minPass).max(maxPass).required().empty().messages({
            'string.base': `password should be a type of 'text'`,
            'string.empty': `password cannot be an empty field`,
            'string.min': 'password should be of minimum 8 characters',
            'string.max': 'password should be of maximum 255 characters',
            'any.required': `password is a required field`}
        ),
        mobile: Joi.string().optional().min(minMobile).regex(/^[0-9]{10}$/).empty().messages({
            'string.base': `mobile should be a type of 'string'`,
            'string.empty': `mobile cannot be an empty field`,
            'any.required': `mobile is a required field`}
        ),
        gender: Joi.string().optional().empty().messages({
            'string.base': `gender should be a type of 'string'`,
            'string.empty': `gender cannot be an empty field`,
            'any.required': `gender is a required field`}
        )}
    )};
