import pool from '../database/database.config';
import winston from 'winston';
import { UserModel } from '../models/users.model';
import { CreateUserDto } from '../dtos/users.dto';

class UserService {

    public users = new UserModel();

    public async create(userData: CreateUserDto,callback: any ){
        pool().getConnection((err, connection) => {
            if(!err) {
                winston.info(`Connected to the database`);
            } else {
                winston.info(err);
            }
            connection.query('INSERT INTO candidate_details set ? ', userData , (error, response) => {
                connection.release();
                
                if (error) {
                    callback(error);
                } else {
                    callback(null, response);
                }
            });
        });
    }
    
    public async isUserExist(email_id: string,callback: any ){
        pool().query('SELECT * FROM candidate_details WHERE email_id = ?', email_id , (err, response) => {
            if (err) {
                callback(err);
            } else {
                callback(null, response);
            }
        });
    }
    
    
}

export default UserService;
