import pool from '../database/database.config';
import winston from 'winston';
import { CreateAuthDto } from '../dtos/auth.dto';

class AuthService {

    public async create(data: CreateAuthDto,callback: any ){
        pool().getConnection((err, connection) => {
            if(!err) {
                winston.info(`Connected to the database`);
            } else {
                winston.info(err);
            }
            connection.query('INSERT INTO login set ? ', data , (error, response) => {
                connection.release();
                
                if (error) {
                    callback(error);
                } else {
                    callback(null, response);
                }
            });
        });
    }
}

export default AuthService;
